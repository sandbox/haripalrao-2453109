<?php
/**
 * @file
 * Show all youtube videos.
 */
?>
<div class="middle_content_video_list"> 
  <div class="full_width fleft mgb_15">
    <?php
    $i = 1;
    foreach ($video_array as $video) :
      if ($i % 2 == 0) :
        $class = 'video_list_right';
      else :
        $class = 'video_list_left';
      endif;
      $vid_actual_url = $video['url'];
      $vid_title = $video['title'];
      $vid_id = $video['video_id'];
      $vid_img = "https://img.youtube.com/vi/" . $vid_id . "/default.jpg";
      if (stristr($vid_actual_url, 'embed')) :
        $vid_url = $vid_actual_url;
      else :
        $chk_vid_url = explode('watch?', $vid_actual_url);
        parse_str($chk_vid_url[1], $output);
        $vid_id = $output['v'];
        $vid_url = "https://www.youtube.com/embed/" . $vid_id;
      endif;
      $youtube_href = $show_videos ? $vid_actual_url : 'javascript:';
      $rel = $show_videos ? '' : $i;
      ?>
      <div class="<?php echo $class; ?>">
        <div class="youtube_v">
          <a class="video_title" target="_blank" rel="<?php echo $rel; ?>" href="<?php echo $youtube_href; ?>">
            <img style="border:none;" src="<?php echo $vid_img; ?>" width="196px" height="110px" alt=""/>
          </a>
        </div>
        <div class="youtube_t">
          <a class="video_title" target="_blank" rel="<?php echo $rel; ?>" href="<?php echo $youtube_href; ?>">
            <?php
            $vt_count = strlen($vid_title);
            if ($vt_count > 55) :
              echo substr($vid_title, 0, 52) . '...';
            else :
              echo $vid_title;
            endif;
            ?>
          </a>
        </div>
        <div class="parent">
          <div style="display:none;" id="<?php echo $i; ?>">
            <a class="closebtn" href="javascript:" rel="<?php echo $i; ?>">[X]</a>
            <iframe title="YouTube video player" type="text/html" width="500" height="330" src="<?php echo $vid_url; ?>?autoplay=0&showinfo=0&rel=0&wmode=transparent&enablejsapi=1" wmode="opaque" frameborder="0"  allowFullScreen></iframe>
          </div>
        </div>
      </div>
      <?php
      $i++;
    endforeach;
    ?>
  </div> 
</div>
