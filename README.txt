
-- SUMMARY --

The module provides a feature to fetch a list of videos.
Such as a list of videos uploaded by a specific user.

-- REQUIREMENTS --

None.

-- CONFIGURATION --

* Configure your user name in Administration » configuration » Youtubev Setting:

  - OR After installation, click on "Configure" link.

-- CREDITS --

Haripal Rao
