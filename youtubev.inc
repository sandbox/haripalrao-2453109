<?php

/**
 * @file
 * Contains youtubev functionality.
 */

/**
 * Implements functionality of youtube user name form.
 *
 * @return array
 *   Return form array.
 */
function youtubev_setting_username() {
  global $base_url;
  $video_gallery_url = $base_url . '/video_gallery';
  $form['youtubev_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Youtube Username'),
    '#default_value' => variable_get('youtubev_username') ? variable_get('youtubev_username') : '',
    '#attributes' => array('placeholder' => 'Enter Your Youtube Username'),
    '#required' => TRUE,
  );
  $form['youtubev_max_videos'] = array(
    '#type' => 'textfield',
    '#title' => t('Max. Videos'),
    '#default_value' => variable_get('youtubev_max_videos') ? variable_get('youtubev_max_videos') : 0,
    '#description' => t('Enter number of videos, you want to show or set zero for no limit.'),
  );
  $form['youtubev_show_videos'] = array(
    '#type' => 'checkbox',
    '#title' => t('Open video in YouTube.'),
    '#default_value' => variable_get('youtubev_show_videos') ? variable_get('youtubev_show_videos') : 0,
    '#description' => t('If enabled, videos will be open within YouTube on clicking of title and thumbnail.'),
  );
  $form['video_gallery_url'] = array(
    '#markup' => '<p>See all videos at: <a target="_blank" href=' . $video_gallery_url . '>' . $video_gallery_url . '</a></p>',
  );
  return system_settings_form($form);
}

/**
 * Functionality to render All youtube videos. And call the tpl file.
 */
function youtubev_video_gallery() {
  drupal_add_css(drupal_get_path('module', 'youtubev') . '/css/youtubev.css');
  drupal_add_js(drupal_get_path('module', 'youtubev') . '/js/youtubev.js');
  $user_name = variable_get('youtubev_username');
  $max_videos = variable_get('youtubev_max_videos');
  $show_videos = variable_get('youtubev_show_videos');
  $get_total_video = youtubev_get_total_youtube_videos($user_name);
  if ($max_videos > $get_total_video) {
    $max_videos = $get_total_video;
  }
  $video_array = array();
  if (!$get_total_video) {
    drupal_set_message(t('No record found! Please check your youtube user name.'));
  }
  else {
    $video_array = youtubev_get_youtube_videos($user_name, $max_videos);
  }
  return theme('youtubev', array('video_array' => $video_array, 'show_videos' => $show_videos));
}

/**
 * Get total number of videos from youtube using your user name.
 *
 * @param string $user_name
 *   User name means your youtube user name.
 *
 * @return int
 *   Return total number of youtube videos uploaded by specific user.
 */
function youtubev_get_total_youtube_videos($user_name) {
  // Check video exists OR not for current user.
  $headers = get_headers(YOUTUBEV_VIDEO_API_URL . $user_name);
  if (!strpos($headers[0], '200')) {
    return FALSE;
  }
  $start_index = 1;
  $total = 0;
  $url = YOUTUBEV_VIDEO_API_URL . $user_name . "/uploads?start-index=" . $start_index;
  $video_details = simplexml_load_file($url);
  if (!empty($video_details->entry[0]->id)) {
    foreach ($video_details as $video) {
      if ($video->title) {
        $total++;
      }
    }
  }
  return $total;
}

/**
 * Get list of youtube videos using youtube user name.
 *
 * @param string $user_name
 *   User name means, your youtube user name.
 * @param int $max_records_val
 *   Max records val means, how many maximum records you want to show.
 *
 * @return array
 *   Return whole array of youtube videos uploaded by specific user.
 */
function youtubev_get_youtube_videos($user_name, $max_records_val) {
  if ($max_records_val) {
    $max_records = "&max-results=" . $max_records_val;
  }
  else {
    $max_records = '';
  }
  $start_index = 1;
  $url = YOUTUBEV_VIDEO_API_URL . $user_name . '/uploads?start-index=' . $start_index . $max_records;
  $video_details = simplexml_load_file($url);
  $video_array = array();
  $i = 0;
  foreach ($video_details as $video) {
    $video_url = (string) $video->link['href'];
    if ($video_url != '') {
      if (strstr($video_url, '?v')) {
        $video_id = youtubev_get_youtube_id($video_url);
        $video_array[$i]['video_id'] = $video_id;
        $video_array[$i]['url'] = $video_url;
        $video_array[$i]['title'] = $video->title;
        $video_array[$i]['content'] = $video->content;
        $i++;
      }
    }
  }
  return $video_array;
}

/**
 * Get youtube video id using youtube url.
 *
 * @param string $video_url
 *   Video url means the youtube vedio url.
 *
 * @return int
 *   Integer value of youtube id.
 */
function youtubev_get_youtube_id($video_url) {
  $video_len = 11;
  $id_starts = strpos($video_url, "?v=");
  if ($id_starts === FALSE) {
    $id_starts = strpos($video_url, "&v=");
  }
  if ($id_starts === FALSE) {
    drupal_set_message(t('YouTube video id not found. Please double-check your URL.'));
  }
  $id_starts += 3;
  $video_id = substr($video_url, $id_starts, $video_len);
  return $video_id;
}
