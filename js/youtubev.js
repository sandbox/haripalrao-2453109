/**
 * @file
 * JQuery for youtube videos pop-up.
 */

jQuery(document).ready(function($) {
  $('.video_title').live('click', function() {
    var vid = $(this).attr('rel');
    $('.parent  div').hide();
    var sel = '#' + vid;
    $(sel).fadeToggle();
    return false;
  });

  // Close youtube video pop-up
  $('.closebtn').click(function() {
    var vid = $(this).attr('rel');
    $('#' + vid).hide();
  });
});
